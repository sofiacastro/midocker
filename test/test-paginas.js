var expect = require ('chai').expect;
var chaijquery =require('chai-jquery');
var request = require ('request');
describe("Pruebas sencillas",function(){
  it ('Test suma', function(){
    expect (9+4).to.equal(13);
  });
});

describe("Pruebas de red",function(){
it ('Test internet', function(done){
  request.get("http://www.forocoches.com",
                function (error,response,body){
                    expect(response.statusCode).to.equal(200);
                    done();
  });
});

it ('Test internet 2', function(done){
  request.get("http://localhost:8081",
                function (error,response,body){
                    expect(response.statusCode).to.equal(200);
                    done();
    });
});

it ('Test internet 3', function(done){
  request.get("http://localhost:8081",
                function (error,response,body){
                    expect(body).contains('<h1>Bienvenido a mi Blog</h1>');
                    done();
    });
});
});

describe("Test contenido html",function(done){
  it ('Test H1', function(done){
    request.get("http://localhost:8081",
                function (error,response,body){
                    expect($('body h1')).to.have.text("Bienvenido a mi blog");
                    done();
    });
});
});
