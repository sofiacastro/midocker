'use strict';

const express = require ('express');
const path = require ('path');

// Constantes
//puerto que quiero usar
const PORT = 8081;

// App

const app = express(); //lo inicializo

app.use(express.static(__dirname));
 //Hasta aqui ya esta escuchando en el puerto 8080, pero no hace nada
 app.get('/', function(req, res){
//Aqui le digo que quiero que haga con el get de la raiz
   //res.send("Bienvenido terricola\n")
   res.sendFile(path.join(__dirname+'/index.html'));
 });

 app.get('/detallePost/:id', function(req, res){
 //Aqui le digo que quiero que haga con el get de la raiz
   //res.send("Bienvenido terricola\n")
   console.log(req);
   res.sendFile(path.join(__dirname+'/detallePost.html'));
 });
 app.get('/admin', function(req, res){
 //Aqui le digo que quiero que haga con el get de la raiz
   //res.send("Bienvenido terricola\n")
   console.log(req);
   res.sendFile(path.join(__dirname+'/admin.html'));
 });

//Para que escuche en el puerto que yo quiero en la constante que he dicho
app.listen(PORT);
console.log('Express funcionando en el puerto' + PORT);
