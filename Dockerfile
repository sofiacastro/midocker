FROM node:boron

#Crear directorio de la app
RUN mkdir -p /usr/src/app
#La carpeta de trabajo va a ser la que acabo de crear
WORKDIR /usr/src/app

#Instalar dependencias
COPY package.json /usr/src/app
#hacer el comando de instalacion
RUN npm install

#empaquetar codigo, ES DECIR QUE META LO QUE FALTA
COPY . /usr/src/app

#TEngo que exponer el puerto que quiero que la gente use desde fuera
EXPOSE 8081

#lanzo la aaplicacion
CMD [ "npm", "start" ]
